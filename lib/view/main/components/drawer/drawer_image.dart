import 'package:flutter/material.dart';

import '../../../../res/constants.dart';

class DrawerImage extends StatelessWidget {
  const DrawerImage({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: 100,
      padding: const EdgeInsets.all(defaultPadding / 6),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          gradient: LinearGradient(
            colors: [
              const Color.fromARGB(255, 241, 176, 198),
              Color.fromARGB(255, 51, 66, 89),
            ],
          ),
          boxShadow: const [
            BoxShadow(
                color: Color.fromARGB(255, 41, 34, 36),
                blurRadius: 10,
                offset: Offset(0, 2)),
            BoxShadow(
                color: Color.fromARGB(255, 151, 170, 186),
                blurRadius: 10,
                offset: Offset(0, -2)),
          ]),
      child: ClipRRect(
          borderRadius: BorderRadius.circular(50),
          child: Transform.rotate(
              angle: 0.1,
              child: Image.asset(
                'assets/images/profile.png',
                fit: BoxFit.cover,
              ))),
    );
  }
}
